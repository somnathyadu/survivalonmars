﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Gamekit2D
{
    public class MovingPlatformCatcher : MonoBehaviour
    {
        MovingPlatform mp;
        PlayerHorizontalMovementTocuhControl pHControl;

        [SerializeField]private Vector2 offset;
        private bool collided = false;
        GameObject player;

        void Start()
        {
            mp = transform.GetComponent<MovingPlatform>();
            if (mp == null)
                Debug.LogError("no mp reference");
            Delay();
        }

        IEnumerator Delay()
        {
            yield return new WaitForEndOfFrame();
            
            pHControl = PlayerHorizontalMovementTocuhControl.playerHControl;
            if (pHControl == null)
                Debug.LogError("no phc found");
        }

        private void OnCollisionStay2D(Collision2D collision)
        {
            if (collision.collider.tag == "Player")
            {
                player = collision.gameObject;
                collided = true;
                player.transform.SetParent(transform);
            }
        }

        private void OnCollisionExit2D(Collision2D collision)
        {
            if (collision.collider.tag == "Player")
            {
                player = collision.gameObject;
                collided = false;
                player.transform.parent = null;
            }

            Debug.Log("Exiting");
        }
        private void Update()
        {
            if(mp == null)
                mp = transform.GetComponent<MovingPlatform>();
            if (player != null)
            {
                bool isJumping = GameMaster.gm.player.GetComponent<Platformer2DPlayerControl>().IsJumping();
                if (collided && !PlayerHorizontalMovementTocuhControl.IsMoving() && !isJumping)
                {
                    player.transform.GetComponent<Rigidbody2D>().MovePosition(player.transform.GetComponent<Rigidbody2D>().position + (mp.Velocity * offset));
                }
            }
        }
    }

}
