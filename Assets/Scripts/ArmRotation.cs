﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArmRotation : MonoBehaviour {
    public int rotationOffset = 0;

    [SerializeField] Transform player;

    private Rigidbody2D p_rb;
    public void Start()
    {
        if (player == null)
            Debug.Log("No player refrence found");
        p_rb = player.GetComponent<Rigidbody2D>();
    }

     private void FixedUpdate()
    {
       //TODO if (!GameMaster.gm.gameIsPaused)
        {
            if (player.localScale.x > 0)
            {
                rotationOffset = 75;
            }
            else
            {
                rotationOffset = 110;
            }
        }
    }

    void LateUpdate () {
       //TODO if (!GameMaster.gm.gameIsPaused)
        {
          if (Weapon.shooting)  /* || p_rb.velocity == new Vector2(0f,0f) this condition is only for mouse control*/
            { 
                     //subtracting the position of mouse from player position
                     Vector2 distance = GameMaster.gm.touchControlReference.Find("TouchScreen/ShootControlVariableJoystick").GetComponent<VariableJoystick>().dir;
                     distance.Normalize();   // normalize the vector . Meaning that all the sum of the vector will be equal to 1.\
                     //Finding the angle between mouse and horizontal and converting it from radian to degree.
                     float rotZ = Mathf.Atan2(distance.y, distance.x) * Mathf.Rad2Deg;
                     transform.rotation = Quaternion.Euler(0f, 0f, rotZ + rotationOffset);
           }
        }
      
    }
}
