﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CharacterMovement : MonoBehaviour {

    private Rigidbody2D rb;

    [SerializeField] private float jumpForce = 400f;
    [SerializeField] private float moveSpeed = 10f;
    [SerializeField] private bool airControl = false;
    [SerializeField] private LayerMask whatIsGround;
    [SerializeField] private bool grounded = false;

    public Text text;


    private Transform groundCheck;
    const float groundedRadios = .2f;

    void Start () {
        rb = GetComponent<Rigidbody2D>();
        groundCheck = transform.Find("GroundCheck");
    }

    private void FixedUpdate()
    {
        grounded = false;

        Collider2D[] colliders = Physics2D.OverlapCircleAll(groundCheck.position, groundedRadios, whatIsGround);
        for(int i = 0; i < colliders.Length; i++)
        {
            if (colliders[i].gameObject != gameObject)
                grounded = true;
        }
    }



    void Update () {


        Move(moveSpeed);

        if (grounded)
        {
            Jump(jumpForce);
            text.text = "True";
        }

        else
            text.text = "False";

    }

    //TO move character left or right
    private void Move(float move)
    {
        float h = Input.GetAxis("Horizontal");
        rb.velocity = new Vector2(h * move, rb.velocity.y);
    }

    private void Jump(float _jumpForce)
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            rb.AddForce(new Vector2(0f, _jumpForce));
        }
    }
}
