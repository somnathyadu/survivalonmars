﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;



public class PlayerJumpControl_Drag : MonoBehaviour, IDragHandler, IPointerUpHandler, IPointerDownHandler
{

    [SerializeField] Vector2 curButtonPos;
    private Vector2 defaultButtonPos;
    private Vector2 buttonPreviousPosition;
   float dragDistanceY = 0f;
    float compareValue = 100f;

    PlayerHorizontalMovementTocuhControl playerHControl;
    RectTransform rt;
    [SerializeField] Transform buttonImageRef;
    [SerializeField] Transform middleX;
   // [SerializeField] Transform jumpPoint;

    [SerializeField] Image goRightImg;
    [SerializeField] Image goLeftImg;
   // [SerializeField] Image jumpImg;



    bool jumping = false;
    Vector3[] limitPos;
    //private float middleX;
    //public Vector2 limitPos;
    private void Start()
    {
        rt = gameObject.GetComponent<RectTransform>();
        playerHControl = gameObject.GetComponent<PlayerHorizontalMovementTocuhControl>();

        defaultButtonPos = buttonPreviousPosition = curButtonPos = buttonImageRef.position;

        {/*
            limitPos = new Vector3[4];
            rt.GetWorldCorners(limitPos);
            // for (int i = 0; i < limitPos.Length; i++) {Debug.Log("World Corner" + i + " : " + limitPos[i]);}

            middleX = limitPos[2].x / 2f;
        */}
        
    }

    public void OnDrag(PointerEventData eventData)
    {
        if (GameMaster.gm.player != null)
        {
            {/*
            dragDistanceY = eventData.position.y - curButtonPos.y;

            //Checking for max limit button can go
            if (eventData.position.y < limitPos[2].y && eventData.position.x < limitPos[2].x)
            {
                //Checking for lowest limit button can go
                if (eventData.position.y > limitPos[0].y && eventData.position.x > limitPos[0].x)
                    buttonImageRef.transform.position = eventData.position;
            }

            //Horizontal movement of player
            if (eventData.position.x >= middleX)
                playerHControl.GoRight();
            else
                playerHControl.GoLeft();

            //make player JUMP
            if (dragDistanceY > compareValue && buttonPreviousPosition.y < eventData.position.y)
            {
                GameMaster.gm.player.GetComponent<Platformer2DPlayerControl>().jump = true;
                jumping = true;
                buttonPreviousPosition = curButtonPos;
                curButtonPos = eventData.position;
            }
            else
            {
                buttonPreviousPosition = curButtonPos = defaultButtonPos;
                jumping = false;
            }
        */
            }
            buttonImageRef.transform.position = new Vector2 (eventData.position.x,buttonImageRef.transform.position.y);
            //For Horizontal movement of player

            if (eventData.position.x >= middleX.position.x)
            {
                playerHControl.GoRight();
               // goLeftImg.color = new Color(goLeftImg.color.r, goLeftImg.color.g, goLeftImg.color.b, 0);
               // goRightImg.color = new Color(goRightImg.color.r, goRightImg.color.g, goRightImg.color.b, 1f);
            }
            else
            {
                playerHControl.GoLeft();
               // goLeftImg.color = new Color(goLeftImg.color.r, goLeftImg.color.g, goLeftImg.color.b, 1f);
               // goRightImg.color = new Color(goRightImg.color.r, goRightImg.color.g, goRightImg.color.b, 0f);
            }

            { // Removing jump function from horizontal control on Alpha Version 0.5.0
                //for jumping
                /* if (jumpPoint.position.y < eventData.position.y && buttonPreviousPosition.y < eventData.position.y)
                 {
                     if (!GameMaster.gm.player.GetComponent<Platformer2DPlayerControl>().IsJumping())
                     {
                         GameMaster.gm.player.GetComponent<Platformer2DPlayerControl>().SetJumpBool();
                         jumping = true;
                         jumpImg.color = new Color(jumpImg.color.r, jumpImg.color.g, jumpImg.color.b, 1f);
                     }
                 }
                 else
                     jumpImg.color = new Color(jumpImg.color.r, jumpImg.color.g, jumpImg.color.b, 0);
                 */
            }
            buttonPreviousPosition = eventData.position;

        }
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        buttonImageRef.gameObject.GetComponent<RectTransform>().anchoredPosition = Vector2.zero;
        buttonPreviousPosition = curButtonPos = buttonImageRef.position;
      //  goLeftImg.color = new Color(goLeftImg.color.r, goLeftImg.color.g, goLeftImg.color.b, 0);
       // goRightImg.color = new Color(goRightImg.color.r, goRightImg.color.g, goRightImg.color.b, 0f); ;
       // jumpImg.color = new Color(jumpImg.color.r, jumpImg.color.g, jumpImg.color.b, 0);
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        buttonImageRef.transform.position = new Vector2(eventData.position.x, buttonImageRef.transform.position.y);
        if (eventData.position.x >= middleX.position.x)
        {
            playerHControl.GoRight();
          //  goLeftImg.color = new Color(goLeftImg.color.r, goLeftImg.color.g, goLeftImg.color.b, 0);
           // goRightImg.color = new Color(goRightImg.color.r, goRightImg.color.g, goRightImg.color.b, 1f);
        }
        else
        {
            playerHControl.GoLeft();
          //  goLeftImg.color = new Color(goLeftImg.color.r, goLeftImg.color.g, goLeftImg.color.b, 1f);
          //  goRightImg.color = new Color(goRightImg.color.r, goRightImg.color.g, goRightImg.color.b, 0f);
        }
    }
}
