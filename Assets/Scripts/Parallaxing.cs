﻿using UnityEngine;

public class Parallaxing : MonoBehaviour
{
    public Transform[] background;
    private float[] parallaxScale;
    public float smoothing = 1f;

    private Transform cam;
    private Vector3 previousCamPos;

    void Awake()
    {
       
    }

    private void Start()
    {
        cam = Camera.main.transform;

        previousCamPos = cam.position;

        parallaxScale = new float[background.Length];

        for (int i = 0; i < background.Length; i++)
        {
            parallaxScale[i] = background[i].position.z * -1;
        }
    }

    void Update()
    {
        float camPosXdiff = previousCamPos.x - cam.position.x;
        for (int i = 0; i < background.Length; i++)
        {
            float parallax = camPosXdiff * parallaxScale[i];

            float backgroundTargetPosX = background[i].position.x + parallax;

            Vector3 backgroundTargetPos = new Vector3(backgroundTargetPosX, background[i].position.y, background[i].position.z);
            background[i].position = Vector3.Lerp(background[i].position, backgroundTargetPos, smoothing * Time.deltaTime);
        }

        previousCamPos = cam.position;
    }
}
