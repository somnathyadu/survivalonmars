﻿using UnityEngine;
using UnityEngine.UI;

public class PlasmaBulletBehaviour : MonoBehaviour {
    
    [SerializeField] Transform bulletHitPrefab;

    [SerializeField] private float damage = 10f;

    private Slider shootSlider;

    private Rigidbody2D rb;
    private Transform firePoint;
    private Vector2 mousePosition;
    private Vector2 dir;

    [SerializeField] private float plasmaBulletYSpeed = 100f;
    [SerializeField] private float plasmaBulletXSpeed = 100f;

    public Vector2 fireQuardinates;
     
    void Start () {
        rb = GetComponent<Rigidbody2D>();
        firePoint = GameMaster.firePoint;
        /*mousePosition = new Vector2(Camera.main.ScreenToWorldPoint(Input.mousePosition).x, Camera.main.ScreenToWorldPoint(Input.mousePosition).y);
        fireQuardinates.x = mousePosition.x - firePoint.transform.position.x;
        fireQuardinates.y = mousePosition.y - firePoint.transform.position.y;*/
        //TODO:: optimization here
        dir = GameMaster.gm.touchControlReference.Find("TouchScreen/ShootControlVariableJoystick").GetComponent<VariableJoystick>().dir;
        if (dir == Vector2.zero)
            dir = new Vector2(1f, 0f);
        fireQuardinates = new Vector2(dir.x, dir.y);
        fireQuardinates.Normalize();
    }
	
	void Update () {
        
        rb.velocity = new Vector2(fireQuardinates.x * plasmaBulletXSpeed, fireQuardinates.y * plasmaBulletYSpeed);
        Destroy(gameObject, 1.5f);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.CompareTag("Enemy"))
        {
            collision.gameObject.GetComponent<DamageEnemy>().Damage(damage, 50);
            Destroy(gameObject, 0.0001f);
        }
        else if (collision.collider != null)
        {
            //Instantiate(bulletHitPrefab, collision.transform.position,Quaternion.identity);
            //print("collided with plateform");
            Destroy(gameObject, 0.0001f);
        }
    }
}
