﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageEnemy : MonoBehaviour {

    [SerializeField] private float enemyHP = 100f;
    Animator anim;
    private void Start()
    {
        anim = GetComponent<Animator>();
    }
    public void Damage(float damage, int hitPoints)
    {
        enemyHP -= damage;
        GameMaster.SetPlayerScore(hitPoints);
        if (enemyHP <= 0)
        {
            GameMaster.SetPlayerScore(hitPoints * 2);
            GameMaster.gm.KillEnemy(gameObject);
        }
    }
}
