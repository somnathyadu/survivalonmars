﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerLaserEye : MonoBehaviour
{
    [SerializeField] float laserDamage = 1;

    private VariableJoystick vJS;

    [SerializeField] Transform trailPrefab;
    [SerializeField] LayerMask whatToHit;
    [SerializeField] Transform distortionParticle;
    [SerializeField] Transform lasserHitPrefab;
    private Transform hitParticle;

    [Header("Audio Files")]
    [SerializeField] string plasmaStart;
    [SerializeField] string plasmaLoop;
    [SerializeField] string plasmaEnd;
    AudioManager audioManager;
    bool playingLaserAudio = false;

    private bool isShooting;

    //Need this variables to draw line
    Transform trail;
    LineRenderer lr;
    Vector2 hitPos;
    Vector2 hitNormal;
    void Start()
    {
        vJS = GameMaster.gm.touchControlReference.Find("TouchScreen/ShootControlVariableJoystick").GetComponent<VariableJoystick>();
        trail = Instantiate(trailPrefab, transform.position, transform.rotation);
        trail.parent = this.transform;
        lr = trail.GetComponent<LineRenderer>();
        audioManager = AudioManager.instance;
        if (audioManager == null)
            Debug.LogError("No audio manager found");
    }


    void Update()
    {
        distortionParticle.gameObject.SetActive(false);
        if (isShooting)
            Shoot();
        else
        {
            audioManager.StopSound(plasmaLoop);
            playingLaserAudio = false;
            trail.gameObject.SetActive(false);
            if (hitParticle != null)
                Destroy(hitParticle.gameObject, 0f);
        }
            
    }

    void Shoot()
    {
        RaycastHit2D raycast = Physics2D.Raycast(transform.position, vJS.dir * 100, 100, whatToHit);
        
        if (raycast.collider != null)
        {
            hitPos = raycast.point;
            hitNormal = raycast.normal;

            if(raycast.collider.gameObject.tag == "Enemy")
                raycast.collider.gameObject.GetComponent<DamageEnemy>().Damage(laserDamage, 1);
        }
        else
        {
            hitPos = vJS.dir;
            hitNormal = new Vector2(9999, 9999);
        }
        if (!playingLaserAudio)
        {
            audioManager.PlaySound(plasmaLoop);
            playingLaserAudio = true;
        }
            
        Effect(hitPos, hitNormal);
    }

    void Effect(Vector2 hitPos, Vector2 hitNormal)
    {

        //To spawn hit particle effect 
        if (hitParticle == null)
        {
            hitParticle = Instantiate(lasserHitPrefab, hitPos, Quaternion.FromToRotation(Vector3.right, hitNormal));
        }
        else if(hitNormal != new Vector2(9999, 9999))
        {
            hitParticle.position = hitPos;
            hitParticle.rotation = Quaternion.FromToRotation(Vector3.right, hitNormal);
        }
        {
            if (!trail.gameObject.activeSelf)
                trail.gameObject.SetActive(true);
           // distortionParticle.gameObject.SetActive(true);
        }
        
        trail.position = transform.position;
        lr.SetPosition(1, transform.position);
        lr.SetPosition(0, hitPos);
        
    }




    public void SetIsShootingBool(bool shoot)
    {
        isShooting = shoot;
    }
}
