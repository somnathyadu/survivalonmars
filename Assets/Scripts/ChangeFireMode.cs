﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using System.Collections;

public class ChangeFireMode : MonoBehaviour, IPointerDownHandler
{
    [SerializeField]Text fMText;
    void IPointerDownHandler.OnPointerDown(PointerEventData eventData)
    {
        PlayerShootControl.pSControl.ChnageFireMode();
        if (PlayerShootControl.pSControl.CurFireMode() == PlayerShootControl.FireMode.LASER)
            fMText.text = "LASER";
        if(PlayerShootControl.pSControl.CurFireMode() == PlayerShootControl.FireMode.PLASMA)
            fMText.text = "PLASMA";
    }

    void Start()
    {
        
    }

    IEnumerator Delay()
    {
        yield return new WaitForSeconds(0.01f);
        if (PlayerShootControl.pSControl.CurFireMode() == PlayerShootControl.FireMode.LASER)
            fMText.text = "LASER";
        if (PlayerShootControl.pSControl.CurFireMode() == PlayerShootControl.FireMode.PLASMA)
            fMText.text = "PLASMA";
    }
}
