﻿using UnityEngine;

public class PlayerHorizontalMovementTocuhControl : MonoBehaviour
{
    [HideInInspector]public static PlayerHorizontalMovementTocuhControl playerHControl;

    public float move = 0;
    bool moving = false;
    //Indicators
    
    // Start is called before the first frame update
    void Start()
    {
        if (playerHControl == null)
            playerHControl = this;
    }

    public void GoLeft()
    {
        move = -1f;
        moving = true;
    }
    public void GoRight()
    {
        move = 1f;
        moving = true;
    }

    public void ResetMove()
    {
        move = 0;
        moving = false;
    }

    public static bool IsMoving()
    {
        return playerHControl.moving;
    }
}
