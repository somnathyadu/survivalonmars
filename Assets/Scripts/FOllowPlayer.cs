﻿using System.Collections;
using UnityEngine;

public class FOllowPlayer : MonoBehaviour {

    Transform player;

    [SerializeField] Transform dontFollowBefor;
    [SerializeField] Transform dontFollowAfter;
    bool searchningForPlayer = false;

    // Use this for initialization
    void Start () {
        transform.position = dontFollowBefor.position;
	}

    private float nextTimeToSearch = 0f;

    [SerializeField] private float lowestYPlayerCanGO = -10f;

    private float timeToWait = 0f;
    
    // Update is called once per frame
    void Update () {
        if (!GameMaster.gm.gameIsPaused)
        {
            if (player != null)
            {
                if (player.position.x > dontFollowBefor.position.x && player.position.x < dontFollowAfter.position.x)
                {
                    //Dont change Z position of this transform or camera will not renderer some sprites
                    Vector3 pos = player.position;
                    pos.z = -10f;
                    transform.position = pos;
                    transform.rotation = player.rotation;
                }
                else
                {
                    float before = Mathf.Abs(transform.position.x - dontFollowBefor.position.x);
                    float after = Mathf.Abs(transform.position.x - dontFollowAfter.position.x);
                    if (before >= after)
                        transform.position = new Vector3(dontFollowAfter.position.x, player.position.y, -10f) ;
                    else
                        transform.position = new Vector3(dontFollowBefor.position.x, player.position.y, -10f);

                }
                if (timeToWait < Time.time)
                {
                    if (player.position.y < lowestYPlayerCanGO)
                    {
                        GameMaster.DamagePlayer(9999999);
                    }
                    timeToWait = Time.time + 1f;
                }
            }
            else
            {
                {
                    if (player == null)
                    {
                        if (!searchningForPlayer)
                        {
                            searchningForPlayer = true;
                            StartCoroutine(SearchPlayer());
                        }
                    }
                    transform.position =new Vector3(0, 0, -10f) ;
                }
            }
        }

	}

    public IEnumerator SearchPlayer()
    {
        GameObject sResult = GameObject.FindGameObjectWithTag("Player");
        if (sResult == null)
        {
            yield return new WaitForSeconds(0.5f);
            StartCoroutine(SearchPlayer());
        }
        else
        {
            player = sResult.transform;
            searchningForPlayer = false;
            yield return null;
        }
    }
}
