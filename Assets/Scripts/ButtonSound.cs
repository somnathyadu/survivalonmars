﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonSound : MonoBehaviour {
    public void OnHoverSound()
    {
        AudioManager.instance.PlaySound("ButtonHover");
    }

    public void OnClickSound()
    {
        AudioManager.instance.PlaySound("ButtonClick");
    }
}
