﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Platformer2DPlayerControl : MonoBehaviour
{
    [HideInInspector]public Platformer2DPlayerControl playerScript;

    [SerializeField] private float walkSpeed = 8f;                    // The fastest the player can travel in the x axis.
    [SerializeField] private float runSpeed = 15f;
    [SerializeField] private float jumpForce = 400f;
    [SerializeField] private bool airControl = false;                 // Whether or not a player can steer while jumping;
    [SerializeField] private LayerMask whatIsGround;                  // A mask determining what is ground to the character


    private Transform groundCheck;    // A position marking where to check if the player is grounded.
    const float groundedRadius = .2f; // Radius of the overlap circle to determine if grounded
    private bool grounded;            // Whether or not the player is grounded.
    private Transform ceilingCheck;   // A position marking where to check for ceilings
    const float ceilingRadius = .01f; // Radius of the overlap circle to determine if the player can stand up
    private Animator anim;            // Reference to the player's animator component.
    private Rigidbody2D rb;
    private bool facingRight = true;  // For determining which way the player is currently facing.

    private bool jump = false;

    //Touch control variable
    private FloatingJoystick fJoystick;
    private PlayerHorizontalMovementTocuhControl playerHControl;
    public float speed;

    private float time;

    void Awake()
    {
        anim = GetComponent<Animator>();
        rb = GetComponent<Rigidbody2D>();
        groundCheck = transform.Find("GroundCheck");
        ceilingCheck = transform.Find("CeilingCheck");
    }

    private void Start()
    {
        //HELP:: joystick control fJoystick = FloatingJoystick.fJoystick;
        //playerHControl = GameMaster.gm.playerHControl;
    }

     void FixedUpdate()
     {
        if (playerHControl == null)
            playerHControl = PlayerHorizontalMovementTocuhControl.playerHControl;
         grounded = false;

         // The player is grounded if a circlecast to the groundcheck position hits anything designated as ground
         Collider2D[] colliders = Physics2D.OverlapCircleAll(groundCheck.position, groundedRadius, whatIsGround);
         for (int i = 0; i < colliders.Length; i++)
         {
             if (colliders[i].gameObject != gameObject)
                 grounded = true;
         }
         anim.SetBool("Ground", grounded);

         anim.SetFloat("vSpeed", rb.velocity.y);

         //Move and jump
         if((grounded || airControl) && !GameMaster.gm.playerIsDead)
         {
             float move = playerHControl.move;
             //HELP:: joystick control rb.velocity = new Vector2(walkSpeed * fJoystick.inputVector.x, rb.velocity.y);
             rb.velocity = new Vector2(walkSpeed * move, rb.velocity.y);
             //HELP:: joystick controlif (fJoystick.inputVector.x > 0.2f)
             if(move > 0)
             {
                 if (!facingRight)
                     Flip();
             }
             //HELP:: joystick control if(fJoystick.Horizontal < 0f)
             else if(move < 0)
             {
                 if (facingRight)
                     Flip();
             }
         }

         if(grounded && jump && !GameMaster.gm.playerIsDead && time < Time.time)
         {
            grounded = false;
            anim.SetBool("Ground", false);
            rb.AddForce(new Vector2(0f, jumpForce));
            jump = false;
            time = Time.time + 0.2f;
         }

         anim.SetFloat("Speed", Mathf.Abs(rb.velocity.x));
         speed = rb.velocity.x;
     }

    public void SetJumpBool()
     {
         if (!jump)
             jump = true;
     }

     void Flip()
     {
         facingRight = !facingRight;
         Vector3 newScale = transform.localScale;
         newScale.x *= -1;
         transform.localScale = newScale;
     }

    public bool IsJumping()
    {
        return jump;
    }
}
