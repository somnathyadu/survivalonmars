﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpitterFakeAI : EnemyFakeAI
{
    //Variable used in shooting
    [SerializeField] private Transform bullet;
    [SerializeField] private Transform spitterFirepoint;
    [SerializeField] private Vector2 spittForce;

    protected float waitForNextAttack = 0f;
    [SerializeField] protected float attackAnimeLength = 0f;

    public override void Attack()
    {
        anim.SetFloat("Speed", Mathf.Abs(rb.velocity.x));
        if (dir.x < 0.15)
        {
            FlipEnemy(dir.x);
        }
        else
        {
            FlipEnemy(dir.x);
        }
        if (waitForNextAttack <= Time.time)
        {
            // TODO:: fix spittForce signs
            anim.SetBool("Attacking", true);
            waitForNextAttack = Time.time + 2f;
        }
        else if (Time.time >= waitForNextAttack - attackAnimeLength)
        {
            anim.SetBool("Attacking",false);
        }

        if (transform.localScale.x > 0)
            spittForce.x = Mathf.Abs(spittForce.x);
        else
            spittForce.x = -Mathf.Abs(spittForce.x);
    }

    protected void Shoot()
    {
        //print("AttackingPlayer");
        Transform clone = Instantiate(bullet, spitterFirepoint.position, spitterFirepoint.rotation);
        clone.GetComponent<Rigidbody2D>().AddForce(spittForce, ForceMode2D.Impulse);
        SpitterBTBehaviour sp = clone.GetComponent<SpitterBTBehaviour>();
        Destroy(clone.gameObject, 2f);
    }

    void Start()
    {
        CallInStart();
        RuntimeAnimatorController ac = anim.runtimeAnimatorController;
        foreach(AnimationClip i in ac.animationClips)
        {
            if (i.name == "SpitterAttack")
            {
                attackAnimeLength = i.length;
                break;
            }
        }
    }

    void Update()
    {
        CallInUpdate();
    }
}
