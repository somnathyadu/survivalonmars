﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpiiteBulleBehaviour : MonoBehaviour {

    [SerializeField] private float damage = 10f;
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.CompareTag("Player"))
        {
           // print("Collided with player");
            GameMaster.DamagePlayer(damage);
        }
        Destroy(gameObject, 0.01f);
    }
}
