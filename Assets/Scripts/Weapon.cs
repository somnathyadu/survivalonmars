﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon : MonoBehaviour {

    private Transform firePoint;
    public float fireRate = 0;
    public int damage = 10;
    public LayerMask whatToHit;
    float timeToFire = 0;

    public GameObject BulletTrailPrefab;
    public Transform MuzzleFlashPrefab;

    //To control CameraShake
    public float shakeAmt = 0.05f;

    AudioManager audioManager;

    float timeToSpawnEffect = 0f;
    public float effectSpwanRate = 10;

    public static bool shooting = false;
    private Animator m_Anime;

    void Start () {
        firePoint = transform.Find("FirePoint");
        if (firePoint == null)
            Debug.LogError("No firePoint found in player");
        m_Anime = gameObject.GetComponentInParent(typeof(Animator)) as Animator;             //GetComponent<Animator>();
        audioManager = AudioManager.instance;
        if (audioManager == null)
            Debug.LogError("No audio manager found");
	}
	
	void Update ()
    {
        if (!GameMaster.gm.gameIsPaused)
        {
            if (fireRate == 0)
            {
                if (shooting)
                {
                    shooting = true;
                    m_Anime.SetBool("Shooting", shooting);
                    if (Time.time > timeToFire)
                    {
                        timeToFire = Time.time + 1 / fireRate;
                        Shoot();
                    }
                }
                else
                {
                    shooting = false;
                    m_Anime.SetBool("Shooting", shooting);
                }
            }
            else
            {
                if (shooting)
                {
                    m_Anime.SetBool("Shooting", shooting);
                    if (Time.time > timeToFire)
                    {
                        timeToFire = Time.time + 1 / fireRate;
                        Shoot();
                    }
                }
                else
                {
                    shooting = false;
                    m_Anime.SetBool("Shooting", shooting);
                }
            }
        }
        
    }

    void Shoot()
    {
        //To get the position of mouse
        Vector2 mousePosition = new Vector2(Camera.main.ScreenToWorldPoint(Input.mousePosition).x, Camera.main.ScreenToWorldPoint(Input.mousePosition).y);
        Vector2 firePointPosition = new Vector2(firePoint.position.x, firePoint.position.y);
        GameObject trail = Instantiate(BulletTrailPrefab, firePoint.position, Quaternion.identity) as GameObject;
        Rigidbody2D rb = trail.GetComponent<Rigidbody2D>();
        Effect();
    }

    void Effect()
    {
        Transform clone = Instantiate(MuzzleFlashPrefab, firePoint.position, firePoint.rotation) as Transform;
        clone.parent = firePoint;
        
        float size = Random.Range(0.6f, 0.9f);
        clone.localScale = new Vector3(size, size, 0);
        Destroy(clone.gameObject, 0.02f);

        /* //Shake camera
        camShake.Shake(shakeAmt, shakeLength);

        //Play shot sound
        audioManager.PlaySound(pistoleSound); */
        audioManager.PlaySound("PlasmaFire");
    }
}
