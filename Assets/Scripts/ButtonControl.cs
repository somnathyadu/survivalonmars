﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonControl : MonoBehaviour
{
    GameMaster gm;
    Platformer2DPlayerControl playerControlScript;
    void Start()
    {
        gm = GameMaster.gm;
    }

    public void SetPlayerJumpBool()
    {
        if(gm.player != null)
        {
            gm.player.GetComponent<Platformer2DPlayerControl>().SetJumpBool();
        }
    }

    public void PauseGame()
    {
        GameMaster.gm.PauseGame();
    }
}
