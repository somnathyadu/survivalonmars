﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class EnemyFakeAI : MonoBehaviour
{

    //Variable used in enemy movement and following player
    [SerializeField] protected Transform minLimit;
    [SerializeField] protected Transform maxLimit;
    [SerializeField] protected Transform maxYLimit;
    [SerializeField] protected Transform minYLimit;
    [SerializeField] protected Vector2 attackRadius = new Vector2(5f,-5f);
    [SerializeField] protected Transform[] canJumpFrom; //Transform point if where enemy reaches he can jump from there
    [SerializeField] protected Vector2[] howFarIsJump; //To make enemy go faster according to distance in x and yaxis
    [SerializeField] protected float damagePlayer;
    [SerializeField] protected float speed;
    protected float normalSpeed; //Just for testing purpose and used in NormalBehaviour() dont delete it
    [SerializeField] protected float spDivider = 4f;
    protected Vector2 dir;
    protected Vector2 _dir;
    protected Animator anim;

    protected bool goingLeft = false;
    protected bool isGoingLeft = false; //use in FlipEnemy()

    protected bool jumping;
    protected GameObject player;

    //Enemies wont ignore even if player is above them for sec of float value
    [SerializeField] float dontIgnoreForSec = 1;
    float time = 0;

    bool searchningForPlayer = false;

    protected AudioManager audioManager;
    [SerializeField]protected string attackSoundName;

    protected Rigidbody2D rb;

    protected float nextTimeToSearch = 0f;

    protected void CallInStart () {
        player = GameObject.Find("Player");
        rb = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
        normalSpeed = speed;

        audioManager = AudioManager.instance;

    }

	protected void CallInUpdate () {
        if (!GameMaster.gm.gameIsPaused)
        {
            // TODO : search for player if it does not exist
            if (player != null)
            {
                //comparing length
                if (minLimit.position.x < player.transform.position.x && maxLimit.position.x > player.transform.position.x)
                {   //comparing height
                    if ( (minYLimit.position.y < player.transform.position.y && maxYLimit.position.y > player.transform.position.y))
                    {
                        FollowPlayer(player.transform);
                        Physics2D.IgnoreLayerCollision(9, 9, true);
                    }
                    else
                    {
                        Physics2D.IgnoreLayerCollision(9, 9, true);
                        BehaveNormal();
                        //time = Time.time + dontIgnoreForSec;
                    }
                }
                else
                {
                    Physics2D.IgnoreLayerCollision(9, 9, true);
                    BehaveNormal();
                }
            }
            else
            {
                Physics2D.IgnoreLayerCollision(9, 9, true);
                BehaveNormal();
                if (!searchningForPlayer)
                {
                    searchningForPlayer = true;
                    StartCoroutine(SearchPlayer());
                }
            }
        }
        else
            rb.velocity = new Vector2(0f, 0f);
	}

  

    void FollowPlayer(Transform _player)
    {
        
            {
                float direction;
                _dir = new Vector2(transform.position.x - _player.position.x, transform.position.y - _player.position.x);
                dir = new Vector2(_dir.x, _dir.y).normalized;
                if (dir.x > 0)
                    direction = -1;
                else
                    direction = 1;
                //Attack Player
                if (_dir.x < attackRadius.x && _dir.x > attackRadius.y) //To shoot if player in range
                {
                    Attack();
                }
                else
                {
                    anim.SetFloat("Speed", Mathf.Abs(rb.velocity.x));
                    anim.SetBool("Attacking", false);
                    //Follow player
                    rb.velocity = new Vector2(direction * speed, rb.velocity.y);
                    //Flip enemy
                    {
                        if (dir.x > 0) //To left
                            isGoingLeft = true;
                        else           //To right
                            isGoingLeft = false;
                        FlipEnemy(isGoingLeft);
                    }

                    // Jump (to make enemy jump)
                    for (int i = 0; i < canJumpFrom.Length; i++)
                    {
                       // to check if enemy is close to any jump position
                        float dist = Vector3.Distance(canJumpFrom[i].position, transform.position);

                        if (dist > -0.6 && dist < 0.6)
                        {
                            rb.velocity = new Vector2(howFarIsJump[i].x, howFarIsJump[i].y);
                           // rb.AddForce(howFarIsJump[i], ForceMode2D.Impulse);
                        }
                    }
                }
            }
    }

    public abstract void Attack();

    protected void BehaveNormal()
    {
        anim.SetFloat("Speed", Mathf.Abs(rb.velocity.x));
        anim.SetBool("Attacking", false);
        // print("Behabin normal");
        if (transform.position.x < minLimit.position.x) //To make enemy go right if he is going left
        {
               
            normalSpeed = speed;
            goingLeft = false;
        }
        else if(transform.position.x > maxLimit.position.x) //To make enemy go left if he is going right
        {
                
               normalSpeed = -speed;
               goingLeft = true;
        }

        if (normalSpeed > 0)
           isGoingLeft = false;
        else
            isGoingLeft = true;

        FlipEnemy(isGoingLeft);
        rb.velocity = new Vector2(normalSpeed, Mathf.Abs(rb.velocity.y));
    }
    protected void FlipEnemy(bool _isGoingLeft) // Overloaded flip when behaving normal
    {
        if (_isGoingLeft && transform.localScale.x > 0) //To flip left
        {
            Vector3 scale = transform.localScale;
            scale.x *= -1;
            transform.localScale = scale;
           // spittForce.x = - Mathf.Abs(spittForce.x);
        }
        else if(!_isGoingLeft && transform.localScale.x < 0 ) // To flip right
        {
            Vector3 scale = transform.localScale;
            scale.x *= -1;
            transform.localScale = scale;
           // spittForce.x = Mathf.Abs(spittForce.x);
        }
        
    }
    protected void FlipEnemy(float x) // Overloaded to flip enemy while attacking
    {
        if (x > 0 && transform.localScale.x > 0) //To flip right
        {
            Vector3 scale = transform.localScale;
            scale.x *= -1;
            transform.localScale = scale;
           // spittForce.x = - Mathf.Abs(spittForce.x);
        }
        else if (x < 0 && transform.localScale.x < 0) // To flip left
        {
            Vector3 scale = transform.localScale;
            scale.x *= -1;
            transform.localScale = scale;
          //  spittForce.x = Mathf.Abs(spittForce.x);
        }

    }


    protected IEnumerator SearchPlayer()
    {
        GameObject sResult = GameObject.FindGameObjectWithTag("Player");
        if (sResult == null)
        {
            yield return new WaitForSeconds(0.5f);
            StartCoroutine(SearchPlayer());
        }
        else
        {
            player = sResult;
            searchningForPlayer = false;
            yield return null;
        }
    }

    protected void PlayAttackSound()
    {
        audioManager.PlaySound(attackSoundName);
    }

    public void DestroyEnemy()
    {
        Destroy(this.transform.parent.gameObject);
    }
}

