﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpitterBTBehaviour : MonoBehaviour {
    Rigidbody2D rb;
    SpitterBTBehaviour sBt;
    Vector2 dir;
	void Start () {
        rb = GetComponent<Rigidbody2D>();
        sBt = this;
	}
	
	
	void Update () {
        rb.AddForce(dir * 20f, ForceMode2D.Impulse);
    }
    public void BulletPath(Vector2 dir)
    {
        this.dir = dir;
    }
}
