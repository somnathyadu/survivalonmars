﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;

public class PlayerShootControl : MonoBehaviour, IPointerUpHandler, IDragHandler
{
    PlayerLaserEye pLEye;
    GameObject player;

    public static PlayerShootControl pSControl;
    VariableJoystick vJS;
    bool shootingLaser = false;
    bool shootingPlasma = false;


    public enum FireMode {PLASMA, LASER};
    FireMode fMode = FireMode.PLASMA;

    [Header("Laser Offset w.r.t. Virtual Joystick DIR variable")]
    [Tooltip("Right X and Y Offset")]
    [SerializeField] Vector2 RXYOffset = new Vector2(0.7f, -0.7f); //Offset value when player facing right
    [Tooltip("Upper Left XY offset")]
    [SerializeField] Vector2 ULXYOffset = new Vector2(-0.8f, 0.7f); //Offset value when player facing left highest x and y can go
    [Tooltip("Lower Left XY offset")]
    [SerializeField] Vector2 LLXYOffset = new Vector2(-1f, -0.6f); //Offset value when player facing left lowest x and y can go

    void Start()
    {
        if (pSControl == null)
            pSControl = this;
        StartCoroutine(Delay());
    }
    IEnumerator Delay()
    {
        yield return new WaitForSeconds(0.01f);
        vJS = VariableJoystick.vJS;
        if (vJS == null)
            Debug.LogError("No VJs reference found");
        player = GameMaster.gm.player;
    }
 
    public void OnDrag(PointerEventData eventData)
    {
        //TODO:: optimization with float comparision
        if (pLEye != null && GameMaster.gm.player != null)
        {
            if (fMode == FireMode.LASER)
            {
                if (player.transform.localScale.x > 0)
                {
                    if (vJS.normal.x > vJS.normal.y) // To shoot whnen player facing right
                    {
                        if (vJS.normal.x > RXYOffset.x && vJS.normal.y > RXYOffset.y)
                        {
                            if (!shootingLaser)
                            {
                                pLEye.SetIsShootingBool(true);
                                shootingLaser = true;
                                Weapon.shooting = false;
                                shootingPlasma = false;
                            }
                        }
                        else
                            StopShootingLaser();
                    }
                    else
                        StopShootingLaser();
                }
                else if (vJS.dir.y > vJS.dir.x) // To shoot when player facing left
                {
                    if (vJS.dir.x < (Mathf.Round(ULXYOffset.x * 100) / 100) && vJS.normal.y < ULXYOffset.y)
                        if (vJS.normal.x > (Mathf.Round(LLXYOffset.x*100)/100) && vJS.normal.y > LLXYOffset.y)
                        {
                            if (!shootingLaser)
                            {
                                pLEye.SetIsShootingBool(true);
                                shootingLaser = true;
                                Weapon.shooting = false;
                                shootingPlasma = false;
                            }
                        }
                        else
                            StopShootingLaser();
                    else
                        StopShootingLaser();
                }
                else
                    StopShootingLaser();

            }
            else //To shoot plasma bullets
            {
                if (!shootingPlasma)
                {
                    Weapon.shooting = true;
                    shootingLaser = false;
                    shootingPlasma = true;
                    pLEye.SetIsShootingBool(false);
                }
            }
        }
    }

    void IPointerUpHandler.OnPointerUp(PointerEventData eventData)
    {
        if (pLEye != null && GameMaster.gm.player != null)
        {
            pLEye.SetIsShootingBool(false);
            Weapon.shooting = false;
            shootingLaser = false;
            shootingPlasma = false;
        }
            
    }

    void Update()
    {
        if (pLEye == null && GameMaster.gm.player != null)
        {
            player = GameMaster.gm.player;
            pLEye = player.transform.Find("body_top_Stand/Neck/LaserEye").GetComponent<PlayerLaserEye>();
        }
    }

    public void ChnageFireMode()
    {
        if (pSControl.fMode == FireMode.PLASMA)
            pSControl.fMode = FireMode.LASER;
        else
            pSControl.fMode = FireMode.PLASMA;

        pLEye.SetIsShootingBool(false);
        Weapon.shooting = false;
        shootingLaser = false;
        shootingPlasma = false;
    }

    public FireMode CurFireMode() // The method will return current fire mode
    {
        return fMode;
    }

    void StopShootingLaser()
    {
            pLEye.SetIsShootingBool(false);
            shootingLaser = false;
    }
   
}
