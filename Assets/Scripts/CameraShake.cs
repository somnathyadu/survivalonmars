﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraShake : MonoBehaviour
{
    Camera mainCam;

    public static CameraShake camShake;

    float shakeAmount = 0;

    public bool shake = false;

    private void Awake()
    {
        if (mainCam == null)
            mainCam = Camera.main;
        if (camShake == null)
            camShake = this;
    }

    private void Update()
    {
     
    }

    public void SetBool(bool _shake)
    {
        shake = _shake;
    }

    public void Shake(float amt, float lenght)
    {
        Debug.Log("Shaking Camera");
        shakeAmount = amt;
        InvokeRepeating("DoShake", 0, 0.01f);
        Invoke("StopShake", lenght);
    }

    void DoShake()
    {
        if (shakeAmount > 0)
        {
            Vector3 camPos = mainCam.transform.position;

            float offsetX = Random.value * shakeAmount * 2 - shakeAmount;
            float offsetY = Random.value * shakeAmount * 2 - shakeAmount;
            //  float offsetZ = Random.value * shakeAmount * 2 - shakeAmount;

            camPos.x += offsetX;
            camPos.y += offsetY;
            //  camPos.z += offsetZ;

            mainCam.transform.position = camPos;

        }
    }

    public CameraShake ReturnInstance()
    {
        return camShake;
    }

    void StopShake()
    {
        CancelInvoke("DoShake");
        mainCam.transform.localPosition = Vector3.zero;
    }
}
