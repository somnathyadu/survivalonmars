﻿using UnityEngine;
using UnityEngine.SceneManagement;
public class PauseMenuUI : MonoBehaviour {


    public void ResumeGame()
    {
        GameMaster.gm.ResumeGame();
    }

    public void RestartLevel()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        GameMaster.gm.gameIsPaused = false;
        gameObject.SetActive(false);
    }

    public void MainMenu()
    {
        GameMaster.gm.gameIsPaused = false;
        SceneManager.LoadScene(0);
    }

    public void ExitGame()
    {
        print("Application quit");
        Application.Quit();
    }
}
