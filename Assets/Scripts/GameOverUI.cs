﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameOverUI : MonoBehaviour {

    public void RestartLevel()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        GameMaster.gm.gameIsPaused = false;
        gameObject.SetActive(false);
    }

    public void MainMenu()
    {
        SceneManager.LoadScene(0);
        GameMaster.gm.gameIsPaused = false;
    }


    public void ExitGame()
    {
        print("Application quit");
        Application.Quit();
    }
}
