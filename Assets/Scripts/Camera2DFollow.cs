using System;
using UnityEngine;

namespace UnityStandardAssets._2D
{
    public class Camera2DFollow : MonoBehaviour
    {
        public Transform target;
        public Camera cameraRef;

        public float minYPosFromPlayersFoot = 4f;

        public float damping = 1;
        public float lookAheadFactor = 3;
        public float lookAheadReturnSpeed = 0.5f;
        public float lookAheadMoveThreshold = 0.1f;
        public float MinCameraPos = -30;
        private float m_OffsetZ;
        private Vector3 m_LastTargetPosition;
        private Vector3 m_CurrentVelocity;
        private Vector3 m_LookAheadPos;


        float zoomSize = 15;


        float nextTimeToSearch = 2;
        // Use this for initialization
        private void Start()
        {
            m_LastTargetPosition = target.position;
            m_OffsetZ = (transform.position - target.position).z;
            transform.parent = null;

        }


        // Update is called once per frame
        private void Update()
        {
            // only update lookahead pos if accelerating or changed direction
            if (target == null) {
                FindPlayer();
                return;
            }

            float xMoveDelta = (target.position - m_LastTargetPosition).x;

            bool updateLookAheadTarget = Mathf.Abs(xMoveDelta) > lookAheadMoveThreshold;

            if (updateLookAheadTarget)
            {
                m_LookAheadPos = lookAheadFactor * Vector3.right * Mathf.Sign(xMoveDelta);
            }
            else
            {
                m_LookAheadPos = Vector3.MoveTowards(m_LookAheadPos, Vector3.zero, Time.deltaTime * lookAheadReturnSpeed);
            }
            Vector3 _target= new Vector3(target.position.x, target.position.y + minYPosFromPlayersFoot, target.position.z);
            Vector3 aheadTargetPos = _target + m_LookAheadPos + Vector3.forward * m_OffsetZ;
            Vector3 newPos = Vector3.SmoothDamp(transform.position, aheadTargetPos, ref m_CurrentVelocity, damping);
            newPos = new Vector3(newPos.x, Mathf.Clamp(newPos.y , MinCameraPos, Mathf.Infinity), newPos.z);
            
            transform.position = newPos;

            m_LastTargetPosition = target.position;



        }

        void FindPlayer()
        {
            if (nextTimeToSearch <= Time.time)
            {
                GameObject searchResult = GameObject.FindGameObjectWithTag("Player");
                if (searchResult != null)
                {
                    target = searchResult.transform;
                    nextTimeToSearch = Time.time + 0.5f;
                }
            }
        }

        void Zoom()
        {
            if (target == null)
                return;
            float playerLastYpos = target.position.y;

            if (target.position.y > 15 && target.position.y < 25)
                if (m_LastTargetPosition.y < target.position.y)
                {
                    zoomSize = Mathf.Lerp(zoomSize, target.position.y, 0.1f);
                    cameraRef.orthographicSize = zoomSize;
                }

            if (target.position.y < 5)
            {
                cameraRef.orthographicSize = Mathf.Lerp(cameraRef.orthographicSize, 15, 0.01f);
            }

        }

        void ZoomOut()
        {
            cameraRef.orthographicSize += 1;
        }
        void ZoomIn()
        {

        }
     }
    
}
