﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class FollowOnEditor : MonoBehaviour
{
    Transform player;
    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.Find("Player").transform;
    }

    // Update is called once per frame
    void Update()
    {
        if(!Application.isPlaying)
            if(transform.position != player.position)
            {
            Vector3 pos = player.position;
            pos.z = -10f;
            transform.position = pos;
            }
    }
}
