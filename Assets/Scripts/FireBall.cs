﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireBall : MonoBehaviour {

    GrassFireWall gF;
    public Transform gFObject;
    Vector2 target;

    bool burn = false;
    bool start = false;
	void Start () {
        //gF = 
        StartCoroutine(wait());
        StartCoroutine(Wait());
        Destroy(gameObject, 6f);
    }
	
	void Update () {
        if(target != null)
        {
            BurnGrass();
        }
        
        
    }
    public void SetTarget(Vector2 d)
    {
        target = d;
    }
    public bool BurnGrass()
    {
        if (start)
        {
            Vector2 dir = new Vector2(target.x - transform.position.x, target.y - transform.position.y);
            if (dir.x <= 0.1f && dir.x <= -0.1f && dir.y <= 5f)
                burn = true;
        }
       
        return burn;
    }
    IEnumerator Wait()
    {
        yield return new WaitForSeconds(4f);
        if(!burn)
           Destroy(gameObject, 0f);
    }
    IEnumerator wait()
    {
        yield return new WaitForSeconds(1f);
        start = true;

    }
    public void Destroy()
    {
        Destroy(gameObject, 3f);
    }
}
