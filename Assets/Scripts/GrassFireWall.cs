﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrassFireWall : MonoBehaviour
{

    public Transform[] grassToBurn;
    public Transform[] leaf;
    public Transform smokeParticle;
    public Transform fireBall;
    public Transform fire;
    public Transform burnTarget;
    public Vector2 dir;
    bool burned = false;
    private Transform clone;
    void Start()
    {
        if (grassToBurn == null)
            Debug.LogError("No grass to burn in GrassFireWall");
        if (!fireBall)
            Debug.LogError("No fireBall in GrassFireWall");
    }

    // Update is called once per frame
    void Update()
    {
        if (!GameMaster.gm.gameIsPaused)
        {
         if (clone && !burned)
                {
                    FireBall fb = clone.gameObject.GetComponent<FireBall>();
                    fb.SetTarget(burnTarget.position);
                    bool burn = fb.BurnGrass();
                    if (burn && grassToBurn != null)
                    {
                
                        foreach (Transform x in grassToBurn)
                        {
                            if(x != null)
                                Destroy(x.gameObject, 3f);
                            Instantiate(fire, x);
                        }
                        Transform cloneSmoke = Instantiate(smokeParticle, burnTarget.position, Quaternion.identity);
                        Destroy(cloneSmoke.gameObject, 10f);
                        foreach (Transform x in leaf)
                        {
                           SpriteRenderer sr = x.gameObject.GetComponent<SpriteRenderer>();
                            sr.color = new Color(0,0,0,1);
                        }
                        fb.Destroy();
                        burned = true;
                    }
                }
        }
       
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (clone == null &&collision.tag == "Bullet")
        {
            //print("OMG grass if burning");
            clone = Instantiate(fireBall, transform.position, transform.rotation);
            clone.SetParent(transform);
        }
    }
}

   
