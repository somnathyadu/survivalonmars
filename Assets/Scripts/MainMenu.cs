﻿using UnityEngine.SceneManagement;
using System.Collections;
using UnityEngine;

public class MainMenu : MonoBehaviour {

    [SerializeField] Transform loadingPanel;
    [SerializeField] Transform panel;


    private void Start()
    {
        StartCoroutine(PlayStartSound());
    }

   

    IEnumerator PlayStartSound()
    {

        yield return new WaitForSeconds(1f);
        if (SceneManager.GetActiveScene().buildIndex == 0)
            AudioManager.instance.PlaySound("MainMenuMusic");
        else
            AudioManager.instance.PlaySound("GameCompleteMusic");
    }
    

    public void StartGame()
    {
        PlayerPrefs.SetInt("PlayerPoints", 0);
        loadingPanel.gameObject.SetActive(true);
        panel.gameObject.SetActive(false);
        SceneManager.LoadSceneAsync(1);
        AudioManager.instance.StopSound("MainMenuMusic");
    }

    public void QuitGame()
    {
        Application.Quit();
    }

    public void MainLevel()
    {
        loadingPanel.gameObject.SetActive(true);
        if(panel != null)
            panel.gameObject.SetActive(false);
        SceneManager.LoadSceneAsync(0);
        AudioManager.instance.StopSound("GameCompleteMusic");
    }

    public void OpenItchIO()
    {
        Application.OpenURL("https://somnathyadu.itch.io/survival-on-mars");
        Debug.Log("Opening SOM on itch io");
    }

    public void OpenTwitter()
    {
        Application.OpenURL("https://twitter.com/yadu_somnath");
        Debug.Log("Opening twitter acc");
    }
}
