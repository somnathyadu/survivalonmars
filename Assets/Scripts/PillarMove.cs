﻿using Cinemachine;
using System.Collections;
using UnityEngine;

public class PillarMove : MonoBehaviour
{
    private Transform pillar;
    private Transform playerRef;

    [SerializeField] Transform crusherLPoint;
    [SerializeField] Transform crusherUPoint;
    [SerializeField] Transform shakeLeftPos;
    [SerializeField] Transform shakeRightPos;

    [SerializeField] float pillarMoveSpeed = 1f;
    [SerializeField] Transform dustPrefabInstntLoc;
    [SerializeField] Transform dustPrefab;

    [SerializeField] float amplitude = 1f;
    [SerializeField] float frequency = 1f;
    [SerializeField] float time = 0.5f;

    private bool goingDown = false;

    GameObject vcam;
    CinemachineVirtualCamera cm;
    CinemachineBasicMultiChannelPerlin noise;
    private bool shaking = false;

    

    private void Start()
    {
        vcam = GameObject.Find("CM vcam1");
        cm = vcam.GetComponent<CinemachineVirtualCamera>();
        noise = cm.GetCinemachineComponent<CinemachineBasicMultiChannelPerlin>();
        if (noise == null)
            Debug.LogError("No vcam in crusher");
        pillar = transform;
    }

    private void Update()
    {
        if(!GameMaster.gm.gameIsPaused &&GameMaster.gm.player != null)
        {
            if (playerRef == null)
                playerRef = GameMaster.gm.player.transform;
            if (pillar.position.y > crusherUPoint.position.y)
                goingDown = true;
            if (pillar.position.y < crusherLPoint.position.y)
            {
                // TODO: include sound of pillar colliding with colliding with ground
                if (!shaking)
                    if (playerRef.position.x > shakeLeftPos.position.x && playerRef.position.x < shakeRightPos.position.x)
                        StartCoroutine(Shake(amplitude, frequency, time));
                goingDown = false;
                Transform clone = Instantiate(dustPrefab, dustPrefabInstntLoc.position, dustPrefabInstntLoc.rotation);
                clone.parent = this.transform;
                Destroy(clone.gameObject, 1f);
            }

            if (goingDown)
                pillar.Translate(Vector2.down * Time.deltaTime * pillarMoveSpeed);
            else
                pillar.transform.Translate(Vector2.up * Time.deltaTime * pillarMoveSpeed);
        }
    }

    private IEnumerator Shake(float amplitude, float frequency, float _time)
    {
        Noise(amplitude, frequency);
        yield return new WaitForSeconds(_time);
        Noise(0, 0);
        shaking  = false;
    }

    public void Noise(float amplitudeGain, float frequencyGain)
    {
        noise.m_AmplitudeGain = amplitudeGain;
        noise.m_FrequencyGain = frequencyGain;
    }
}
