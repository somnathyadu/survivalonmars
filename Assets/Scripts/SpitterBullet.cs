﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpitterBullet : MonoBehaviour {

    public Transform bulletPrefab;

    private Rigidbody2D rb;
    private Transform firePoint;
    private Vector2 mousePosition;

    EnemyFakeAI enemyFakeAI;

    [SerializeField] private float BulletYSpeed = 100f;
    [SerializeField] private float BulletXSpeed = 100f;

    Vector2 fireQuardinates;

    void Start()
    {
        enemyFakeAI = GetComponent<EnemyFakeAI>();
        firePoint = transform.Find("SpitterFirePoint");
        if (firePoint == null)
            Debug.LogError("No firePoint found in enemy object");
    }

    void Update()
    {

    
    }

    public void InstBullet()
    {
        Transform clone = Instantiate(bulletPrefab);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider != null)
            Destroy(gameObject);

    }
}
