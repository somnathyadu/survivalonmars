﻿using UnityEngine;

public class T_Crusher : MonoBehaviour
{
    [SerializeField] Transform crusherLPoint;
    [SerializeField] Transform crusherUPoint;

    [SerializeField] float pillarMoveSpeed = 1f;

    [SerializeField] Transform[] pillar;
    [SerializeField] Transform[] dustPrefabInstntLoc;
    [SerializeField] Transform dustPrefab;

    private static CameraShake camShake;

    public bool[] goingDown;

    void Start()
    {
        camShake = CameraShake.camShake.GetComponent<CameraShake>();
        if (camShake == null)
            Debug.LogError("No came shake intance in T_CRUsher");

        goingDown = new bool[pillar.Length];
        for (int i = 0; i < pillar.Length; i++)
        {
            goingDown[i] = false;
        }
    }
 
    void Update()
    {
        for (int i = 0; i < pillar.Length; i++)
        {
            MovePillar(pillar[i], dustPrefabInstntLoc[i], ref goingDown[i]);
        }
    }

    void MovePillar(Transform _pillar, Transform dustPreLoc, ref bool _goingDown)
    {
        if (_pillar.position.y > crusherUPoint.position.y)
            _goingDown = true;
        if (_pillar.position.y < crusherLPoint.position.y)
        {
            // TODO: include sound of pillar colliding with colliding with ground
            camShake.Shake(0.2f,0.1f);
            _goingDown = false;
            Transform clone = Instantiate(dustPrefab, dustPrefab.position, dustPreLoc.rotation);
            clone.parent = this.transform;
            Destroy(clone.gameObject, 0.5f);
        }
           
        if (_goingDown)
            _pillar.Translate(Vector2.down * Time.deltaTime * pillarMoveSpeed);
        else
            _pillar.transform.Translate(Vector2.up * Time.deltaTime * pillarMoveSpeed);
        return;
    }
}
