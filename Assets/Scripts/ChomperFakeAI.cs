﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChomperFakeAI : EnemyFakeAI
{

    public override void Attack()
    {
        anim.SetFloat("Speed", Mathf.Abs(rb.velocity.x));
        if (dir.x < 0.15)
            FlipEnemy(dir.x);
        else
            FlipEnemy(dir.x);
        anim.SetBool("Attacking",true);
    }

    void DamagePlayer()
    {
        GameMaster.DamagePlayer(damagePlayer);
    }

    void Start()
    {
        CallInStart();
    }

    void Update()
    {
        CallInUpdate();
    }
}


