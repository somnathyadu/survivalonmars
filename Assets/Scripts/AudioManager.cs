﻿using UnityEngine;

[System.Serializable]
public class Sound
{
    public string name;
    public AudioClip clip;

    public bool isPlaying = false;

    [Range(0f, 1f)]
    public float volume = 0.7f;
    [Range(0.5f, 1.5f)]
    public float pitch = 1f;

    [Range(0f, 0.5f)]
    public float randomVolume = 0.1f;
    [Range(0f, 0.5f)]
    public float randomPitch = 0.1f;

    public bool loop = false;

    private AudioSource source;

    public void SetSource(AudioSource _source)
    {
        source = _source;
        source.clip = clip;
        source.loop = loop;
        if (source == null)
            Debug.LogError("No audio source found");
        if (_source == null)
            Debug.LogError("No audio _source found");
    }

    public void Play()
    {
        source.volume = volume * (1 + Random.Range(-randomVolume / 2f, randomVolume / 2f));
        source.pitch = pitch * (1 + Random.Range(-randomPitch / 2f, randomPitch / 2f));
        source.Play();
        isPlaying = true;
    }

    public void Stop()
    {
        source.Stop();
        isPlaying = false;
    }
}

public class AudioManager : MonoBehaviour
{

    public static AudioManager instance;

    [SerializeField]
    Sound[] sounds;

    private void Awake()
    {
        if (instance != null)
        {
            if (instance != this)
            {
                Destroy(this.gameObject);
            }
        }
        else
        {
            instance = this;
            DontDestroyOnLoad(this);
        }
        for (int i = 0; i < sounds.Length; i++)
        {
            GameObject _gm = new GameObject("Sound_" + i + "_" + sounds[i].name);
            _gm.transform.SetParent(this.transform);
            sounds[i].SetSource(_gm.AddComponent<AudioSource>());
        }
        //print(Time.time);
        if (this.gameObject == null)
            Debug.LogError("Audio manager not initialized");
    }

    public void PlaySound(string _name)
    {
        for (int i = 0; i < sounds.Length; i++)
        {
            if (sounds[i].name == _name)
            {
                sounds[i].Play();
                return;
            }
        }
        //no sound with _name
        Debug.LogWarning("No sound found with name, " + _name + " in AudioManager");
    }

    public void StopSound(string _name)
    {
        for (int i = 0; i < sounds.Length; i++)
        {
            if (sounds[i].name == _name)
            {
                sounds[i].Stop();
                return;
            }

            //no sound with _name
            Debug.LogWarning("No sound found with name, " + _name + " in AudioManager");
        }
    }
}
