﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingPlatformCustom : MonoBehaviour
{
    [SerializeField] Transform nextPoint;

    Vector3 prevPos;

    bool goingNextPoint = true;
    void Start()
    {
        prevPos = transform.position;
    }

    void Update()
    {
        if ((transform.position.x < nextPoint.position.x || transform.position.y < nextPoint.position.y) && goingNextPoint)
        {
            transform.Translate(nextPoint.position.normalized * Time.deltaTime);
        }
    }
}
