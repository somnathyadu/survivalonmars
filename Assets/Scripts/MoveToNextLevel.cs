﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;


public class MoveToNextLevel : MonoBehaviour {

    [SerializeField] Transform playerPos;
    [SerializeField] Transform point;
    //[SerializeField] Vector2 offset;
    bool checkingPos = false;
    float timeToWait = 0f;
    bool loadingScene = false;
    int nextsceneIndex;

    //[SerializeField] Slider slider;
    //[SerializeField] Text text;
    [SerializeField] Transform panel;


	// Update is called once per frame
	void Update () {
        if (!GameMaster.gm.gameIsPaused)
        {
            if (timeToWait < Time.time && !loadingScene)
                    {
                        //Vector2 dist = playerPos.position - point.position;
                        //dist = new Vector2(Mathf.Abs(dist.x), Mathf.Abs(dist.y));
                       // if (dist.x < offset.x && dist.x > -offset.x && dist.y < offset.y && dist.y > -offset.y)
                        if (playerPos.position.x > point.position.x)
                        {
                           nextsceneIndex = SceneManager.GetActiveScene().buildIndex + 1;
                           print("Loading new level");
                           LoadLevel(nextsceneIndex);
                           bool loadingScene = true;
                        }
                        timeToWait = Time.time + 0.3f;
                    }
        }
        
	}

    public void LoadLevel(int buildIndex)
    {
        panel.gameObject.SetActive(true);
        SceneManager.LoadScene(buildIndex);

        //slider.gameObject.SetActive(true);
        //StartCoroutine(LoadAsynchronously(buildIndex));


    }

    IEnumerator LoadAsynchronously(int buildIndex)
    {
        AsyncOperation operation = SceneManager.LoadSceneAsync(buildIndex);


        
         operation.allowSceneActivation = false;
         float progress;
         while (operation.isDone == false)
         {
           print(operation.progress);
           print(operation.isDone);
           progress = Mathf.Clamp01(operation.progress / 0.9f);
           //slider.value = progress;
           //text.text = progress * 100 + "%";
         if (operation.progress >= 0.89f)
            {
                operation.allowSceneActivation = true;
                bool loadingScene = false;
                operation.allowSceneActivation = true;
                yield return null;
            }
        } 
      
    }

}
